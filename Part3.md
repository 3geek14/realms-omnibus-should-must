::::: {.rule .block .part-3}
### 3.1 &sect; [Limb Shots](./Omnibus.html#limb-shots) ###

If you are struck in a limb, your PC loses the use of that entire limb. If you
are struck in a limb that has already been lost, and that limb blocked what
could have possibly been a legal shot to another location, then that location
[should]{.highlight} be considered hit. You cannot protect the side of your PC's body with a
disabled arm or by lifting a disabled leg up to block. Once your PC's limb has
been disabled, it should be put behind you. This keeps it out of the way as well
as provides a visual cue to other players that your PC is hurt. Once you have
lost a limb, your PC cannot use that limb at all. Don't limp on a damaged leg.

<details class="edit" open><summary></summary>
If you
are struck in a limb that has already been lost, and that limb blocked what
could have possibly been a legal shot to another location, then that location
~~should be~~ [is]{.new} considered [to be]{.new} hit.
</details>
:::::

::::: {.ideal .block .part-3}
### 3.1 &sect; [Limb Shots](./Omnibus.html#limb-shots) ###

If you are struck in a limb, your PC loses the use of that entire limb. If you
are struck in a limb that has already been lost, and that limb blocked what
could have possibly been a legal shot to another location, then that location
should be considered hit. You cannot protect the side of your PC's body with a
disabled arm or by lifting a disabled leg up to block. Once your PC's limb has
been disabled, it [should]{.highlight} be put behind you. This keeps it out of the way as well
as provides a visual cue to other players that your PC is hurt. Once you have
lost a limb, your PC cannot use that limb at all. Don't limp on a damaged leg.
:::::

::::: {.rule .block .part-3}
### 3.2 &sect; [Off-Target Areas](./Omnibus.html#off-target-areas) ###

Your face and the front of your throat are off-target; players [should]{.highlight} never aim
attacks there. If you are hit in either location, you should announce it even
though the hit has no game effect. Face is considered the area on your head
below your eyebrows, in front of your ears. Throat is considered the
forward-facing section of your neck, above the sternum. The forehead from the
eyebrows up, the back and top of your head, and the sides and back of your neck
are all legal targets.

<details class="edit" open><summary></summary>
~~Your~~ [The]{.new} face and the front of ~~your~~ [the]{.new} throat are
off-target; ~~players should~~ never aim attacks there.
</details>
:::::

::::: {.rule .block .part-3}
### 3.2 &sect; [Off-Target Areas](./Omnibus.html#off-target-areas) ###

Your face and the front of your throat are off-target; players should never aim
attacks there. If you are hit in either location, you [should]{.highlight} announce it even
though the hit has no game effect. Face is considered the area on your head
below your eyebrows, in front of your ears. Throat is considered the
forward-facing section of your neck, above the sternum. The forehead from the
eyebrows up, the back and top of your head, and the sides and back of your neck
are all legal targets.

<details class="edit" open><summary></summary>
If you are hit in either location, you ~~should~~ [need to]{.new} announce it[,]{.new} even
though the hit has no game effect.
</details>
:::::

::::: {.rule .block .part-3}
### 3.2 &sect; [Off-Target Areas](./Omnibus.html#off-target-areas) ###

You [should]{.highlight} never deliberately aim for an off-target area (face with any weapon,
or head with a ranged weapon) or the groin or breasts. Accidents happen, but if
you frequently target these sensitive areas it will indicate to others that you
are not a safe and controlled fighter, and you may be asked to sit out by a
marshal.

<details class="edit" open><summary></summary>
~~You should never~~ [Never]{.new} deliberately aim for an off-target area (face with any weapon,
or head with a ranged weapon) or the groin or breasts.
</details>
:::::

::::: {.suggestion .block .part-3}
### 3.3 &sect; [Calling Your Armor](./Omnibus.html#calling-your-armor) ###

Armor protects by hit location, so if you have more than one piece of armor on a
hit location, it is all considered damaged when you are struck there. On the
other hand, if one piece of armor covers more
than one hit location, it is treated as separate hit locations. The armor hit
locations are divided up by the hit locations for taking wounds and kills. Light
armor allows you to call out "Armor 1" on that hit location and the next blow
will do damage. If you are wearing heavy armor you [should]{.highlight} call out "Armor 1"
then "Armor 2" before the hit location takes damage.

<details class="edit" open><summary></summary>
If you are wearing heavy armor you ~~should~~ [can]{.new} call out "Armor 1" [and]{.new}
then "Armor 2" before the hit location takes damage.
</details>
:::::

::::: {.rule .block .part-3}
### 3.3 &sect; [Calling Your Armor](./Omnibus.html#calling-your-armor) ###

You can be wearing armor on a hit location yet still have some areas of that hit
location that are not covered. These sections are also considered armored if at
least 75% of the entire hit location is covered by armor. If some of that armor
is light and some is heavy then the uncovered areas can only call out "Armor 1".
If all of the armor on the hit location is heavy then then the uncovered areas
can call both points of armor. A marshal [should]{.new} be asked to rule whether or not
75% of the location is covered in any case where it is ambiguous.

<details class="edit" open><summary></summary>
[In any case where it's close, ask a marshal]{.new} ~~A marshal should be
asked~~ to rule whether or not 75% of the location is covered ~~in any case
where it is ambiguous~~.
</details>
:::::

::::: {.rule .block .part-3}
### 3.4 &sect; [Death](./Omnibus.html#death) ###

If your PC is dead or soulless, you [should]{.highlight} lie or sit still. Try not to look
around or talk. Do your best to role-play a corpse. Don't get upset if someone
hits you with a killing blow when you are already dead. If somebody does this
just say, "Dead." They are making sure that you really are dead.

<details class="edit" open><summary></summary>
If your PC is dead or soulless, ~~you should~~ [try to]{.new} lie or sit still.
</details>
:::::

::::: {.rule .block .part-3}
### 3.4 &sect; [Death](./Omnibus.html#death) ###

In tournaments, or other high combat situations, it is acceptable for a
character to move out of the way to avoid being stepped on. They may resume
their death act in a safer place. They may also sit or kneel to avoid injuries.
If you are role-playing death in any of these forms, you [should]{.highlight} put your weapon
over your head to signify your character is dead.

<details class="edit" open><summary></summary>
If you are role-playing death in any of these forms, ~~you should put~~ [try to
keep]{.new} your weapon over your head to signify your character is dead.
</details>
:::::

::::: {.rule .block .part-3}
### 3.4 &sect; [Playing Dead](./Omnibus.html#playing-dead) ###

It is legal for characters to lie down and pretend they are dead, but they may
not put their weapons over their head. If someone asks a player if their
character is dead, the player and the character are not obligated to answer, but
if a player is asked to describe their character's wounds, they [should]{.highlight} do so as
accurately and honestly as possible. If their character is not really dead and
someone comes close to them to loot their character's body, they are free to
attack the unsuspecting looters. If you are unsure as to whether someone's
character is dead and want the character to be, tap the player gently in a kill
location.

<details class="edit" open><summary></summary>
but if a player is asked to describe their character's wounds, they ~~should~~
[must]{.new} do so as accurately and honestly as possible.
</details>
:::::

::::: {.rule .block .part-3}
### 3.5 &sect; [Destroying a Body](./Omnibus.html#destroying-a-body) ###

Characters that are pretending to be dead [should]{.highlight} interpret any body destroying
blows as blows to the closest kill location. So, if your PC is pretending to be
dead and someone starts to destroy their body, the first blows that are struck
on the ground next to you should be played as if they were striking your PC on
the nearest kill location, destroying any undamaged armor, and then killing
them.

<details class="edit" open><summary></summary>
~~Characters that are~~ [If your character is]{.new} pretending to be
dead[,]{.new} ~~should~~ interpret any body destroying blows as blows to the
closest kill location.
</details>
:::::

::::: {.rule .block .part-3}
### 3.5 &sect; [Destroying a Body](./Omnibus.html#destroying-a-body) ###

Characters that are pretending to be dead should interpret any body destroying
blows as blows to the closest kill location. So, if your PC is pretending to be
dead and someone starts to destroy their body, the first blows that are struck
on the ground next to you [should]{.highlight} be played as if they were striking your PC on
the nearest kill location, destroying any undamaged armor, and then killing
them.

<details class="edit" open><summary></summary>
the first blows that are struck on the ground next to you ~~should be played~~
[count]{.new} as if they were striking your PC on the nearest kill location,
</details>
:::::

::::: {.rule .block .part-3}
### [3.7: Weapon Rules](./Omnibus.html#weapon-rules) ###

Players are responsible for being safe with the weapon(s) they are using. Before
using a weapon style in-game, players [should]{.highlight} take it upon themselves to be
properly trained by a marshal or someone who is safe and proficient with that
weapon style.

<details class="edit" open><summary></summary>
players ~~should take it upon themselves to~~ [must]{.new} be properly trained
by a marshal or someone who is safe and proficient with that weapon style.
</details>
:::::

::::: {.rule .block .part-3}
### 3.7 &sect; [Wielding Weapons](./Omnibus.html#wielding-weapons) ###

If you are wielding an illegal weapon or combination and you attack, then you
[should]{.highlight} tell the target "Don't take that." If something you are not wielding
blocks a shot, then you should treat the blow as if it had landed. If you are
not sure where that blow would have landed, then assume it would hit the
location that would cause you the most harm (e.g., an unarmored kill location).
There are special restrictions for spellcasters in terms of wielding and
blocking with different weapons, and specific consequences for doing so outside
of the spellcaster's restriction. See [Breaking Weapon
Restriction](./Omnibus.html#breaking-weapon-restriction) for more information.

<details class="edit" open><summary></summary>
If you are wielding an illegal weapon or combination and you attack, then you
~~should~~ [must]{.new} tell the target "Don't take that."
</details>
:::::

::::: {.rule .block .part-3}
### 3.7 &sect; [Wielding Weapons](./Omnibus.html#wielding-weapons) ###

If you are wielding an illegal weapon or combination and you attack, then you
should tell the target "Don't take that." If something you are not wielding
blocks a shot, then you [should]{.highlight} treat the blow as if it had landed. If you are
not sure where that blow would have landed, then assume it would hit the
location that would cause you the most harm (e.g., an unarmored kill location).
There are special restrictions for spellcasters in terms of wielding and
blocking with different weapons, and specific consequences for doing so outside
of the spellcaster's restriction. See [Breaking Weapon
Restriction](./Omnibus.html#breaking-weapon-restriction) for more information.

<details class="edit" open><summary></summary>
If something you are not wielding blocks a shot, then you ~~should~~
[must]{.new} treat the blow as if it had landed.
</details>
:::::

::::: {.rule .block .part-3}
### 3.7 &sect; [Bows](./Omnibus.html#bows) ###

Bows must have a full draw weight of 30 pounds or less. Just like melee weapons,
you [should]{.highlight} be careful on how hard your arrows are striking your opponent. Arrows
should be drawn with the minimal pull necessary to score a successful hit.

<details class="edit" open><summary></summary>
Just like melee weapons, ~~you should~~ be careful on how hard your arrows are
striking your opponent.
</details>
:::::

::::: {.rule .block .part-3}
### 3.7 &sect; [Bows](./Omnibus.html#bows) ###

Bows must have a full draw weight of 30 pounds or less. Just like melee weapons,
you should be careful on how hard your arrows are striking your opponent. Arrows
[should]{.highlight} be drawn with the minimal pull necessary to score a successful hit.

<details class="edit" open><summary></summary>
~~Arrows should be drawn~~ [Fire arrows]{.new} with the minimal pull necessary
to score a successful hit.
</details>
:::::

::::: {.suggestion .block .part-3}
### [3.8: Weapons Construction](./Omnibus.html#weapon-construction) ###

There are several ways to make weapons in the Realms. If you are playing for the
first time, it [might]{.highlight} be a better idea to borrow weapons than try to make any of
your own. Once you see what other weapons look like and ask people about how
they made their weapons, you will be better prepared to construct your own.

<details class="edit" open><summary></summary>
If you are playing for the first time, it ~~might be~~ [is]{.new} a better idea
to borrow weapons than try to make any of your own.
</details>
:::::

::::: {.rule .block .part-3}
### [3.8: Weapons Construction](./Omnibus.html#weapon-construction) ###

* Only magic weapons [should]{.highlight} be made with a blue-colored striking surface, and
  you may only make a magic weapon through the use of spells in the magic
  system, or if you are releasing one as an EH.

<details class="edit" open><summary></summary>
Only magic weapons ~~should~~ [may]{.new] be made with a blue-colored striking
surface,
</details>
:::::

::::: {.rule .block .part-3}
### 3.8 &sect; [Arrows, Javelins, and other Missile Weapons](./Omnibus.html#arrows-javelins-and-other-missile-weapons) ###

* All aqua tube shafts [should]{.highlight}, at minimum, be covered by two lengths of
  strapping tape.

<details class="edit" open><summary></summary>
All aqua tube shafts ~~should~~ [must]{.new}, at minimum, be covered by two
lengths of strapping tape.
</details>
:::::

::::: {.rule .block .part-3}
### 3.8 &sect; [Arrows, Javelins, and other Missile Weapons](./Omnibus.html#arrows-javelins-and-other-missile-weapons) ###

* Lightning Bolt props are the only "arrow" props that [should]{.highlight} be made solely
  from white duct tape. Arrows may have a white head or shaft, but not both.

<details class="edit" open><summary></summary>
Lightning Bolt props are the only "arrow" props that ~~should~~ [may]{.new} be
made solely from white duct tape. Arrows may have a white head or shaft, but not
both.
</details>
:::::

::::: {.rule .block .part-3}
### [3.9: Shield Construction](./Omnibus.html#shield-construction) ###

* All shields must have their edges covered by foam. Any protruding metal screws
  or bolts [should]{.highlight} also be padded.

<details class="edit" open><summary></summary>
Any protruding metal screws or bolts ~~should~~ [must]{.new} also be padded.
</details>
:::::

::::: {.rule .block .part-3}
### [3.10: Equipment Inspections](./Omnibus.html#equipment-inspections) ###

You must inspect any armor, weapon, or shield before using it. If you are unsure
about an item's safety, ask a marshal and they will inspect it for you. Any item
can be inspected at any time during an event at anyone's request. This is meant
for the purpose of ensuring safety and [should]{.highlight} never be used for strategic or
tactical purposes. The EH or a designated marshal retains final ruling on the
approval for use of any armor, weapon, or shield.

<details class="edit" open><summary></summary>
This is meant for the purpose of ensuring safety and ~~should~~ [may]{.new}
never be used for strategic or tactical purposes.
</details>
:::::

::::: {.rule .block .part-3}
### 3.11 &sect; [Poison](./Omnibus.html#poison) ###

There may be occasions where a PC is struck with a blow and the wielder calls
"Poison." If this strike damages a PC in any location, be it torso, arm, leg,
etc., then the PC is killed. When the blow does no damage to a PC, such as a hit
to an off-target area or a hit to armor that protects the PC, then the poison
has no effect. Armor struck with a poison blow is still used. If a PC is under
the effects of a spell that protects them from poison in some fashion, the PC
still takes the normal damage from the blow but the poison will have no
additional effect. Players [should]{.highlight} also call "Immunity to Poison," to allow an
opponent to understand that you recognized that the blow was poisoned.

<details class="edit" open><summary></summary>
Players ~~should~~ [must]{.new} also call "Immunity to Poison," to allow an
opponent to understand that you recognized that the blow was
poisoned.
</details>
:::::

::::: {.rule .block .part-3}
### [3.12: Non-Combatant Players](./Omnibus.html#non-combatant-players) ###

* First, and most importantly, a non-combatant [should]{.highlight} never be hit by a weapon,
  and intentionally doing so is cheating and should be reported to a marshal.
  Instead, to kill a non-combatant, a player may simply point at them and say
  "die", provided either the player is wielding a melee weapon and is within 10
  feet of them, or the player is wielding a missile weapon. This can not be
  resisted by any means. If a non-combatant is regenerating, you can simply
  point at them and say "impale" and, while within 10 feet of the body, they
  will be considered impaled. They may be rendered soulless as normal.

<details class="edit" open><summary></summary>
First, and most importantly, [never hit]{.new} a non-combatant ~~should never be
hit by~~ [with]{.new} a weapon,
</details>
:::::

::::: {.rule .block .part-3}
### [3.12: Non-Combatant Players](./Omnibus.html#non-combatant-players) ###

* First, and most importantly, a non-combatant should never be hit by a weapon,
  and intentionally doing so is cheating and [should]{.highlight} be reported to a marshal.
  Instead, to kill a non-combatant, a player may simply point at them and say
  "die", provided either the player is wielding a melee weapon and is within 10
  feet of them, or the player is wielding a missile weapon. This can not be
  resisted by any means. If a non-combatant is regenerating, you can simply
  point at them and say "impale" and, while within 10 feet of the body, they
  will be considered impaled. They may be rendered soulless as normal.

<details class="edit" open><summary></summary>
intentionally doing so is cheating ~~and should be reported to a marshal~~.
</details>
:::::