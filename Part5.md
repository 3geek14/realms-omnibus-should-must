::::: {.ideal .block .part-5}
### [5.1: The Social Structure](./Omnibus.html#the-social-structure) ###

The Realms is not governed by a single kingdom. Each nation has its own
hierarchy and structure. There are no hard rules for governing the social
structure. Claiming land and titles is anyone's prerogative. The social
structure really only has one rule: if you can back up your title or claim then
you deserve to hold it. If you can't, then you [should]{.highlight} have nothing to complain
about if you get put in your place.
:::::

::::: {.ideal .block .part-5}
### [5.2: Creating a Character](./Omnibus.html#creating-a-character) ###

If you are already familiar with role-playing in general, or with live action
role-playing specifically, you probably already know how to make a character. If
you are new to the concept of role-playing, the following questions might help
you establish the traits and characteristics of your PC. You [should]{.highlight} try to
answer the questions for yourself, but some suggestions are provided.
:::::

::::: {.rule .block .part-5}
### 5.4 &sect; [Spellbooks](./Omnibus.html#spellbooks) ###

A player may choose to or need to replace their character's spellbook. This
could be due to such situations as loss, damage, illegibility, or a simple
desire to improve it. The player [should]{.highlight} create a new spellbook with contents as
close as possible to the prior one. In the case of swapping out an old spellbook
which is still available, the required information in it must be copied over to
the new one. In the case of the old spellbook being unavailable or unreadable,
then the player must make their best effort to recreate the information in the
original. At the next event where they play that character, they must inform the
Event Holder or Magic Marshal and have it inspected. At the beginning of the
spellbook, where their names and weapon restriction is written, they should note
that they are replacing their spellbook along with the date and the signature of
the inspecting Event Holder or Magic Marshal.

<details class="edit" open><summary></summary>
The player ~~should~~ [may]{.new} create a new spellbook with contents as close
as possible to the prior one.
</details>
:::::

::::: {.rule .block .part-5}
### 5.4 &sect; [Spellbooks](./Omnibus.html#spellbooks) ###

A player may choose to or need to replace their character's spellbook. This
could be due to such situations as loss, damage, illegibility, or a simple
desire to improve it. The player should create a new spellbook with contents as
close as possible to the prior one. In the case of swapping out an old spellbook
which is still available, the required information in it must be copied over to
the new one. In the case of the old spellbook being unavailable or unreadable,
then the player must make their best effort to recreate the information in the
original. At the next event where they play that character, they must inform the
Event Holder or Magic Marshal and have it inspected. At the beginning of the
spellbook, where their names and weapon restriction is written, they [should]{.highlight} note
that they are replacing their spellbook along with the date and the signature of
the inspecting Event Holder or Magic Marshal.

<details class="edit" open><summary></summary>
At the beginning of the spellbook, where their names and weapon restriction is
written, they ~~should~~ [must]{.new} note that they are replacing their
spellbook along with the date and the signature of the inspecting Event Holder
or Magic Marshal.
</details>
:::::

::::: {.suggestion .block .part-5}
### 5.4 &sect; [Armor](./Omnibus.html#armor-restriction) ###

A spellcaster who uses armor (calls "Armor" in response to a blow) in violation
of their armor restriction is immediately faced with the same consequences as
one who has broken their weapon restriction, and [should]{.highlight} refer to that section
for details.

<details class="edit" open><summary></summary>
and ~~should~~ [they can]{.new} refer to that section for details.
</details>
:::::