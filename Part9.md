::::: {.rule .block .part-9}
### 9.2 &sect; [Arbitration Committee](./Omnibus.html#arbitration-committee) ###

The Arbitration Committee [should]{.highlight} strive to bring issues to resolution within 30 
days of a complaint being filed. If they are unable to do so within 45 days, they
must notify the Event Holders mailing list of the charges and the reason why they've
been unable to come to a decision.

<details class="edit" open><summary></summary>
The Arbitration Committee ~~should~~ [shall]{.new} strive to bring issues to
resolution within 30 days of a complaint being filed.
</details>
:::::

::::: {.rule .block .part-9}
### 9.2 &sect; [Arbitration Committee](./Omnibus.html#arbitration-committee) ###

The report [should]{.highlight} be made publicly available on
[RealmsNet](https://www.realmsnet.net/arbitration).

<details class="edit" open><summary></summary>
The report ~~should~~ [must]{.new} be made publicly available on
[RealmsNet](https://www.realmsnet.net/arbitration).
</details>
:::::