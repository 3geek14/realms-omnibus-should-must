::::: {.rule .block .part-7}
### [7.1: Becoming A Realms Event-Holder](./Omnibus.html#becoming-a-realms-event-holder) ###

* It [should]{.highlight} be open to the public.

<details class="edit" open><summary></summary>
It ~~should~~ [must]{.new} be open to the public.
</details>
:::::

::::: {.rule .block .part-7}
### [7.1: Becoming A Realms Event-Holder](./Omnibus.html#becoming-a-realms-event-holder) ###

* If the EH(s) are under the age of 18, they must have an adult(s) either as a
  co-EH(s) or as a Listed Contact(s) in the event description. One of the said
  adults [should]{.highlight} be on-site for the event.

<details class="edit" open><summary></summary>
One of the said adults ~~should~~ [must]{.new} be on-site for the event.
</details>
:::::

::::: {.rule .block .part-7}
### [7.1: Becoming A Realms Event-Holder](./Omnibus.html#becoming-a-realms-event-holder) ###

As an Event Holder you [are expected to]{.highlight}:

<details class="edit" open><summary></summary>
As an Event Holder you ~~are expected to~~ [must]{.new}:
</details>
:::::