::::: {.suggestion .block .part-1}
### 1.4 &sect; [Registration](./Omnibus.html#registration) ###

A list of events can be found on realmsnet.net under the Realms Calendar or the
Events tab. Whenever you plan on attending an event, you [should]{.highlight} register for it
through realmsnet.net as some events may have caps on how many players can
attend, while others may have different registration requirements or pre-event
information to disperse to players who are planning on attending. Registering
for events also helps give the event staff an idea of how many players they
might expect, and helps them to plan accordingly. It is also important to
register any dietary or medical restrictions you may have.

<details class="edit" open><summary></summary>
Whenever you plan on attending an event, you ~~should~~ [are encouraged
to]{.new} register for it through realmsnet.net[,]{.new}
</details>
:::::