---
title: 'Should vs. Must'

...

<form id="show-hide">
	<label for="part">Filter by Omnibus Part:</label>
	<select name="part" id="part">
		<option value="0">Display All</option>
		<option value="1">Part 1</option>
		<option value="2">Part 2</option>
		<option value="3">Part 3</option>
		<option value="4">Part 4</option>
		<option value="5">Part 5</option>
		<option value="6">Part 6</option>
		<option value="7">Part 7</option>
		<option value="8">Part 8</option>
		<option value="9">Part 9</option>
	</select>
	
	<label for="type">Filter by Type:</label>
	<select name="type" id="type">
		<option value="all">Display All</option>
		<option value="rule">Rule (Red)</option>
		<option value="suggestion">Suggestion (Blue)</option>
		<option value="ideal">Ideal (Yellow)</option>
		<option value="change">Change (Not Yellow)</option>
	</select>
	
	Show/Hide Edits:
	<button type="button" id="show-edits" name="show-edits">Show All</button>
	<button type="button" id="hide-edits" name="hide-edits">Hide All</button>
</form>

<script type="text/javascript">
var form = document.querySelector("#show-hide");
form.addEventListener("change", function(event) {
	var body = document.querySelector("body");
	
	var show_part = new FormData(form).get("part");
	body.classList
		.remove("show-part-1", "show-part-2", "show-part-3", "show-part-4", "show-part-5", "show-part-6", "show-part-7", "show-part-8", "show-part-9");
	if (show_part !== "0") {
		body.classList.add("show-part-" + show_part);
	}
	
	var show_type = new FormData(form).get("type");
	body.classList
		.remove("show-rule", "show-suggestion", "show-ideal", "show-change");
	if (show_type !== "all") {
		body.classList.add("show-" + show_type);
	}
	
	event.preventDefault();
}, false);

var showAll = document.getElementById('show-edits');
showAll.addEventListener('click', function(event) {
	var details = document.querySelectorAll('details');
	Array.from(details).forEach(function(obj, idx) {
		obj.open = true;
	});
}, false);

var hideAll = document.getElementById('hide-edits');
hideAll.addEventListener('click', function(event) {
	var details = document.querySelectorAll('details');
	Array.from(details).forEach(function(obj, idx) {
		obj.open = false;
	});
}, false);
</script>